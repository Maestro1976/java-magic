package Decision.InputStrim;
/*
Программа должна считывать c консоли имя файла.
Программа должна выводить на экран содержимое файла.
Поток чтения из файла (FileInputStream) должен быть закрыт.
BufferedReader также должен быть закрыт.
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadTheFileNameFromTheConsole
{
	public static void main(String[] args) throws IOException
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String fileName = reader.readLine();
		try (FileInputStream fileInputStream = new FileInputStream(fileName))
		{
			int letter;
			while ((letter = fileInputStream.read()) != -1)
			{
				System.out.print((char) letter);
			}
			reader.close();
		}
		catch (Exception myException)
		{
			myException.printStackTrace();
		}
	}
}
