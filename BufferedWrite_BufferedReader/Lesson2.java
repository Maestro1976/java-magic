package BufferedWrite_BufferedReader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Integer.parseInt;
/*
В этой задаче тебе нужно:
Ввести имя файла с консоли.
Прочитать из него набор чисел.
Вывести в консоли только четные, отсортированные по возрастанию.
Входные данные записанные в файл NumberText.txt:
5
8
-2
11
3
-5
2
10
Пример вывода:
-2
2
8
10
Требования:
•	Программа должна считывать данные с консоли.
•	Программа должна создавать FileInputStream для введенной с консоли строки.
•	Программа должна выводить данные на экран.
•	Программа должна вывести на экран все четные числа, считанные из файла, отсортированные по возрастанию.
•	Программа должна закрывать поток чтения из файла — FileInputStream.
 */

public class Lesson2
{
    public static void main(String[] args) throws IOException
    {
        List<Integer> list = new ArrayList<>();

        BufferedReader readerFileName = new BufferedReader(new InputStreamReader(System.in));
        String nameFile = readerFileName.readLine();

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(nameFile))))
        {
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                if (parseInt(line) % 2 == 0)
                {
                    list.add(parseInt(line));
                }
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        Collections.sort(list);
        for (Integer print : list)
        {
            System.out.println(print);
        }
    }
}
