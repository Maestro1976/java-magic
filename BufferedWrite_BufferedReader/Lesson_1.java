package BufferedWrite_BufferedReader;
import java.io.*;
/*
В этой задаче тебе нужно:
Прочесть с консоли имя файла.
Считывать строки с консоли, пока пользователь не введет строку "exit".
Вывести абсолютно все введенные строки в файл, каждую строчку — с новой строки.

Программа должна считывать c консоли имя файла.
Создай и используй объект типа BufferedWriter.
Программа не должна ничего считывать из файловой системы.
Программа должна считывать строки с консоли, пока пользователь не введет строку "exit".
Программа должна записать все введенные строки (включая "exit") в файл: каждую строчку — с новой строки.
Метод main должен закрывать объект типа BufferedWriter после использования.
Метод main не должен выводить данные на экран.

вводимые данные:
C:\Users\DIMA\ProgrammingTraining\TestSolution\Test.txt
 */

public class Lesson_1
{
	public static void main(String[] args)
	{
		//передаем данные из меода "readingAFileFromTheConsole" в строку "fileName"
		String fileName = readingAFileFromTheConsole();
		methodWrite(fileName);
		methodReader(fileName);
	}

	//метод считывания название файла с консоли
	public static String readingAFileFromTheConsole()
	{
		//создали переменную строку для приема названия файла с консоли
		String fileName = null;
		//подключаем консоль для считывания данных
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		//обработка исключений
		try
		{
			//данные с консоли передаем в переменную "String fileName"
			fileName = reader.readLine();
		}
		//работа с выявленными исключениями
		catch (IOException exceptionInMethodReadingAFileFromTheConsole)
		{
			exceptionInMethodReadingAFileFromTheConsole.printStackTrace();
		}
		//передаем данные с консоли "fileName" в строку "String readingAFileFromTheConsole"
		return fileName;
	}

	//метод записывания строк в файл "fileName"
	public static void methodWrite(String fileName)
	{
		//ключевое слово для прерывания процесса ввода строк
		String textStopTheProgram = "exit";
		//проверка на правильность ввода пути для записи файла
		try (
				//запись в файл
				BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
				//чтение с консоли
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
		)
		{
			//переменная для записи строки с консоли
			String document;
			//бесконечный цикл
			while (true)
			{
				//если в веденной строке нету слова "exit" то записываем эту строку в файл, каждое слово с новой строки, иначе
				if (!(document = br.readLine()).equals(textStopTheProgram))
				{
					//записываем строку в файл с новой строчки
					bw.write(document + "\n");
					//очищаем буфер ввода
					bw.flush();
				}
				//в цикле если введеная строка содержит слово "exit" то записываем это слово в файл и прерываем запись
				else
				{
					//записываем контрольное слово в общий файл
					bw.write(textStopTheProgram);
					break;
				}
			}
		}
		//работа с выявленными исключениями
		catch (IOException ex)
		{
			System.out.println(ex.getMessage());
		}
	}

	//метод чтения файла
	public static void methodReader(String writeDocument)
	{
		//передаем целиковый файл в буффер для считывания
		try (BufferedReader br = new BufferedReader(new FileReader(writeDocument)))
		{
			//перменная для записи строки из файла
			String text;
			//в цикле если считываемая строка не равна пустоте то выводим строку в консоль
			while ((text = br.readLine()) != null)
			{
				//вывод строки в консоль, каждая строка с новой строчки
				System.out.print(text + "\n");
			}
		}
		//работа с выявленными исключениями
		catch (Exception myException)
		{
			myException.printStackTrace();
		}
	}
}
