package RobotBattle;

public interface Attackable
{
	BodyPart attack();
}
