package RobotBattle;

public interface Defensable
{
	BodyPart defense();
}
